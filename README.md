# Case Study 1: Product Liking Functionality [Ecommerce] (in progress)

### Once you have finalized design you have to:
1. Provide information about database type which you are opting RDBMS/NoSQL/GRAPH?
- NoSQL
2. Provide information about database why you selected?
- scalability
- fast because of distributed database and processing
3. Provide schema details along with Primary/Partition/Composite/Clustering keys?
-
### Extension to above problem:
4. Get all products liked by a user should also return product names
5. Get all user names who have liked any products
 
